#!/usr/bin/env node

const installDeps = JSON.parse((process.env.PANDEMICS_DEPS || 'true').toLowerCase());

if (!installDeps) {
  console.error('Skipping pandemics depencies install.');
  // sucess, shortcircuit
  process.exit(0);
} else {
  // fail, continue ||
  process.exit(1);
}
