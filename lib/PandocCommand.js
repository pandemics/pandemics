const assets = require('../lib/assets.js');

module.exports = class PandocCommand {
  constructor () {
    this.command = assets.bin['pandoc'];
  }

  pushOption (option, key, value) {
    this.command += ` --${option}`;
    if (key) {
      if (value) {
        this.command += ` ${key}="${value}"`;
      } else {
        this.command += ` "${key}"`;
      }
    };
  }

  pushString (s) {
    this.command += ` ${s}`;
  }

  pushVariable (name, value) {
    this.pushOption('variable', name, value);
  }

  pushBibliography (bibfile) {
    this.pushOption('metadata', 'bibliography', bibfile);
  }

  pushFilter (filter) {
    if (filter.endsWith('.lua')) {
      this.pushOption('lua-filter', filter);
    } else {
      this.pushOption('filter', filter);
    }
  }

  toString () {
    return this.command;
  }

  toArray () {
    return this.command.split(/\s/);
  }
}
