const path = require('path');
const findup = require('findup-sync');
const assets = require('../lib/assets.js');

module.exports = () => {

  // start from default env
  // ---------------------------------------------------------------------------
  const env = Object.assign(process.env);

  // add binaries provided as node modules, eg. cross-env
  // ---------------------------------------------------------------------------
  const nodeModulesBinPath = findup(
    'node_modules/.bin',
    { cwd: __dirname }
  );
  
  env.PATH = nodeModulesBinPath + path.delimiter + env.PATH;

  // add compiled binaries
  // ---------------------------------------------------------------------------
  env.PATH = path.dirname(assets.bin['pandoc']) + path.delimiter + env.PATH;
  env.PATH = path.dirname(assets.bin['pandoc-crossref']) + path.delimiter + env.PATH;

  // return updated env
  // ---------------------------------------------------------------------------
  return env;
};
