const path = require('path');
const { spawnSync } = require('child_process');

module.exports = function (sourceDir) {
  // run git command in the source folder and return child
  function spawnGit (cmd) {
    return spawnSync(
      'git', cmd.split(/\s/),
      {cwd: sourceDir}
    )
  }
  // run git diff in the source directory. The --exit-code mode will only return
  // 0 if there are no diffs in the repo
  var sourceIsDirty = spawnGit('diff --quiet --exit-code').status
  // if clean, get the current commit SHA
  if (sourceIsDirty === 0) {
    return spawnGit('rev-parse --short HEAD').stdout.toString().trim()
  }
}
