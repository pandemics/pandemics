function get_timer () {

  var t;

  function start () {
    t = process.hrtime.bigint ();
  }

  function stop () {
    let elapsed = Math.round (Number ((process.hrtime.bigint () - t) / 1000000n));
    t = undefined;
    return elapsed;
  }

  return {start, stop};
}


module.exports = get_timer()
