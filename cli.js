#!/usr/bin/env node

/* Load dependencies */
/* ========================================================================== */

// CLI framework
const prog = require('caporal');

// actions associated with respective commands
const publisher = require('./publisher');

/* Program description */
/* ========================================================================== */
const { version } = require('./package.json');

prog
  .version(version)
  .description('Academic writing without stress.');

/* Program actions */
/* ========================================================================== */

// publish
prog
  .command('publish', 'Compile a manuscript using pandoc')
  .option('--to <recipe>', 'Template to use for compiling')
  .option('-f, --format <ext>', 'Destination format extension')
  .option('-o, --output <target>', 'Destination filename')
  .argument('[sources...]', 'Source .md file', /\.md$/)
  .action(publisher);

/* Put everithing to action */
/* ========================================================================== */
prog.parse(process.argv);
